using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Services.DTO;
using Core.Execeptions;
using API.ViewModels;
using Domain.Entities;
using Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using API.Utilities;

namespace API.Controllers
{
    [ApiController]
    public class ItemController : ControllerBase
    {

        private readonly IItemService _itemService;
        private readonly IMapper _mapper;


        public static IWebHostEnvironment _webHostEnviroment;

        public ItemController(IItemService itemService, IMapper mapper, IWebHostEnvironment webHostEnviroment)
        {
            _itemService = itemService;
            _mapper = mapper;
            _webHostEnviroment = webHostEnviroment;
        }



        [HttpPost]
        [Route("/api/v1/itens/create")]
        public async Task<IActionResult> Create([FromForm] CreateItemViewModel itemViewModel)
        {
            try
            {
                var itemDTO = _mapper.Map<ItemDTO>(itemViewModel);

                var itemCreated = await _itemService.Create(itemDTO);


                return Ok(new ResultViewModel
                {
                    Message = "item Adicionado a WishList",
                    Success = true,
                    Data = itemCreated
                });

            }
            catch (DomainExecptions ex)
            {

                return BadRequest(Responses.DomainErrorMessage(ex.Message, ex.Errors));
            }
            catch (Exception)
            {
                return StatusCode(500, Responses.ApplicationErrorMessage());
            }

        }


        [HttpPut]
        [Route("/api/v1/itens/update")]

        public async Task<IActionResult> Update([FromForm] UpdateItemViewModel itemViewModel)
        {
            try
            {
                var item = await _itemService.Get(itemViewModel.Id);

                if (item == null)
                    return Ok(new ResultViewModel
                    {
                        Message = "Nenhum item foi encontrado com o ID Informado!",
                        Success = true,
                        Data = item
                    });

                var itemDTO = _mapper.Map<ItemDTO>(itemViewModel);
                var itemUpdate = await _itemService.Update(itemDTO);


                return Ok(new ResultViewModel
                {
                    Message = "Item atualizado!",
                    Success = true,
                    Data = itemUpdate
                });

            }
            catch (DomainExecptions ex)
            {

                return BadRequest(Responses.DomainErrorMessage(ex.Message, ex.Errors));
            }
            catch (Exception)
            {
                return StatusCode(500, Responses.ApplicationErrorMessage());
            }

        }


        [HttpGet]
        [Route("/ap1/v1/users/get-item/{id}")]

        public async Task<IActionResult> Get(long id)
        {
            try
            {
                var item = await _itemService.Get(id);

                if (item == null)
                    return Ok(new ResultViewModel
                    {
                        Message = "Nenhum item foi encontrado com o ID Informado!",
                        Success = true,
                        Data = item
                    });

                return Ok(new ResultViewModel
                {
                    Message = "Item Encontrado!",
                    Success = true,
                    Data = item
                });
            }
            catch (DomainExecptions ex)
            {

                return BadRequest(Responses.DomainErrorMessage(ex.Message, ex.Errors));
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }


        [HttpDelete]
        [Route("/api/v1/users/remove/{id}")]

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                await _itemService.Remove(id);

                return Ok(new ResultViewModel
                {
                    Message = "Item Removido da WhisList",
                    Success = true,
                    Data = null
                });

            }
            catch (DomainExecptions ex)
            {
                return BadRequest(Responses.DomainErrorMessage(ex.Message, ex.Errors));
            }
            catch (Exception)
            {
                return StatusCode(500, Responses.ApplicationErrorMessage());
            }

        }


        [HttpGet]
        [Route("/api/v1/users/random-item")]

        public async Task<IActionResult> RandomByItem()
        {
            try
            {
                var itemRandom = await _itemService.RandomByItem();

                return Ok(new ResultViewModel
                {
                    Message = "Item encontrado!",
                    Success = true,
                    Data = itemRandom
                });

            }
            catch (DomainExecptions ex)
            {
                return BadRequest(Responses.DomainErrorMessage(ex.Message, ex.Errors));
            }
            catch (Exception)
            {
                return StatusCode(500, Responses.ApplicationErrorMessage());
            }
        }

    }
}