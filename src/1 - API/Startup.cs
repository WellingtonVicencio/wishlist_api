using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using API.ViewModels;
using Domain.Entities;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Infrastructure.Repositories;
using Services.DTO;
using Services.Interfaces;
using Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System.Text;
using Pomelo.EntityFrameworkCore.MySql;

namespace API
{
    public class Startup
    {


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public readonly ItemContext _context;
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region AutoMapper

            var autoMapperConfig = new MapperConfiguration(cfg =>
           {
               cfg.CreateMap<Item, ItemDTO>().ReverseMap();
               cfg.CreateMap<CreateItemViewModel, ItemDTO>().ReverseMap();
               cfg.CreateMap<UpdateItemViewModel, ItemDTO>().ReverseMap();
           });

            services.AddSingleton(autoMapperConfig.CreateMapper());
            #endregion
            // injeção de Dependencia
            #region DI

            services.AddSingleton(d => Configuration);

            services.AddCors();
            services.AddDbContext<ItemContext>(options => options
             .UseMySql(Configuration.GetConnectionString("USER_MANAGER")), ServiceLifetime.Transient);
            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<ItemRepository, ItemReopository>();

            #endregion

            #region Swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "WishList.API",
                    Version = "v1",
                    Description = "API Rest para Teste pratico em Backend na qiTech",
                    Contact = new OpenApiContact
                    {
                        Name = "Wellington Vicencio",
                        Email = "wellington.vicencio@uni9.edu.br",
                        Url = new Uri("https://www.linkedin.com/in/wellington-vicencio-396655181/"),

                    },
                });



            });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json"
                            , "Manager.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}