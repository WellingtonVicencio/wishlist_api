CREATE TABLE `item` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Link` varchar(255) DEFAULT NULL,
  `Foto` varchar(255) DEFAULT NULL,
  `Ganhou/Comprou` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COLLATE='latin1_swedish_ci';

INSERT IGNORE INTO product (Id, Title, Description, Link, Foto, Ganhou/Comprou)
VALUES
(1, "Mouse", "MouseGamer Personalizado", "https://translate.google.com/", "C:\WishList\src\1 - API\38e5f278-c46f-4deb-ab81-68086478f85f_Captura de Ecrã (1).png", "1"),
(1, "Mesa", " Personalizado", "https://translate.google.com/", "C:\WishList\src\1 - API\38e5f278-c46f-4deb-ab81-68086478f85f_Captura de Ecrã (1).png", "1"),
(1, "MouseApoio", "MouseGamer Personalizado", "https://translate.google.com/", "C:\WishList\src\1 - API\38e5f278-c46f-4deb-ab81-68086478f85f_Captura de Ecrã (1).png", "1"),
(1, "Rato", "MouseGamer Personalizado", "https://translate.google.com/", "C:\WishList\src\1 - API\38e5f278-c46f-4deb-ab81-68086478f85f_Captura de Ecrã (1).png", "1");