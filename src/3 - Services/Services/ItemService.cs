using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Interfaces;
using Services.DTO;
using Core.Execeptions;
using Services.Interfaces;
using System.IO;
using System;

namespace Services.Services
{
    public class ItemService : IItemService
    {
        private readonly IMapper _mapper;
        private readonly ItemRepository _itemRepository;

        public ItemService(IMapper mapper, ItemRepository itemRepository)
        {
            _mapper = mapper;
            _itemRepository = itemRepository;
        }

        public async Task<ItemDTO> Create(ItemDTO itemDTO)
        {
            var itemExists = await _itemRepository.GetT(itemDTO.Title);

            string unifiqueFileName = null;
            if (itemDTO.Photos != null)
            {
                string uploadFolder = Path.Combine(@"", Directory.GetCurrentDirectory());
                unifiqueFileName = Guid.NewGuid().ToString() + "_" + itemDTO.Photos.FileName;
                string filePath = Path.Combine(uploadFolder, unifiqueFileName);
                itemDTO.PhotoUrl = filePath;
            }


            if (itemExists != null)
                throw new DomainExecptions("Já existe o item cadastrado na lista de lista de desejos com o titulo informado.");

            var item = _mapper.Map<Item>(itemDTO);
            item.Validate();


            var itemCreated = await _itemRepository.Create(item);

            return _mapper.Map<ItemDTO>(itemCreated);
        }

        public async Task<ItemDTO> Update(ItemDTO itemDTO)
        {
            var itemExists = await _itemRepository.GetT(itemDTO.Title);

            string unifiqueFileName = null;
            if (itemDTO.Photos != null)
            {
                string uploadFolder = Path.Combine(@"", Directory.GetCurrentDirectory());
                unifiqueFileName = Guid.NewGuid().ToString() + "_" + itemDTO.Photos.FileName;
                string filePath = Path.Combine(uploadFolder, unifiqueFileName);
                itemDTO.PhotoUrl = filePath;
            }


            if (itemExists != null)
                throw new DomainExecptions("Já existe o item cadastrado na lista de lista de desejos com o titulo informado.");

            var item = _mapper.Map<Item>(itemDTO);
            item.Validate();


            var itemCreated = await _itemRepository.Update(item);

            return _mapper.Map<ItemDTO>(itemCreated);
        }

        public async Task<ItemDTO> Get(long id)
        {
            var item = await _itemRepository.Get(id);

            return _mapper.Map<ItemDTO>(item);
        }

        public async Task<List<ItemDTO>> RandomByItem()
        {
            var ItemRandom = await _itemRepository.RandomByItem();

            return _mapper.Map<List<ItemDTO>>(ItemRandom);
        }

        public async Task Remove(long id)
        {
            var itemExists = await _itemRepository.Get(id);

            if (itemExists != null)
                await _itemRepository.Remove(id);
            else
                throw new DomainExecptions("ID não Encontrado");
        }

    }
}