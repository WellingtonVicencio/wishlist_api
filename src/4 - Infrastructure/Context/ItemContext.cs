using Domain.Entities;
using Infrastructure.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;

namespace Infrastructure.Context
{
    public class ItemContext : DbContext
    {

        public ItemContext()
        {

        }

        public ItemContext(DbContextOptions<ItemContext> options) : base(options)
        {

        }


        public DbSet<Item> Items { get; set; }



     /*    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(@"Server=127.0.0.1;Database=wishlistapi;Uid=porps12;Pwd=porpeta12;");
        } */





        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ItemMap());
        }

    }
}