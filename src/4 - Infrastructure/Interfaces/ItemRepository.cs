using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Infrastructure.Interfaces
{
    public interface ItemRepository : IBaseRepository<Item>
    {
        Task<List<Item>> RandomByItem();
        Task<Item> GetT(string title);

    }
}