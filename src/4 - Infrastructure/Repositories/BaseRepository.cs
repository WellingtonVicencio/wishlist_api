using System.Linq;
using Infrastructure.Context;
using System.Threading.Tasks;
using Domain.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Infrastructure.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : Base
    {


        private readonly ItemContext _context;

        public BaseRepository()
        {
        }

        public BaseRepository(ItemContext context)
        {
            _context = context;
        }

        public virtual async Task<T> Create(T obj)
        {
            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {


                    _context.Add(obj);
                    await _context.SaveChangesAsync();

                    transaction.Commit();
                    return obj;
                }

            }
        }

        public virtual async Task<T> Get(long id)
        {
            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {


                    var obj = await _context.Set<T>()
                                               .AsNoTracking()
                                               .Where(x => x.Id == id)
                                               .ToListAsync();

                    transaction.Commit();

                    return obj.FirstOrDefault();
                }
            }
        }
        public virtual async Task<List<T>> Get()
        {

            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {
                    transaction.Commit();
                    return await _context.Set<T>()
                                         .AsNoTracking()
                                         .ToListAsync();

                }
            }
        }


        public virtual async Task Remove(long id)
        {
            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {

                    var obj = await Get(id);

                    if (obj != null)
                    {
                        _context.Remove(obj);
                        await _context.SaveChangesAsync();
                    }
                    transaction.Commit();
                }
            }
        }

        public virtual async Task<T> Update(T obj)
        {

            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {
                    _context.Entry(obj).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                   transaction.Commit();
                    return obj;
                }
            }
        }
    }
}