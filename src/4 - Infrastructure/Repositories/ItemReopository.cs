using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class ItemReopository : BaseRepository<Item>, ItemRepository
    {
        private readonly ItemContext _context;

        public ItemReopository(ItemContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<Item>> RandomByItem()
        {

            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {
                    var Rnd = new Random();
                    int toSkip = Rnd.Next(0, _context.Items.Count());

                    var item = await _context.Items
                        .OrderBy(x => Guid.NewGuid())
                        .Skip(toSkip)
                        .Take(1)
                        .ToListAsync();

                    transaction.Commit();

                    return item;

                }
            }
        }



        public async Task<Item> GetT(string title)
        {

            using (var db = _context)
            {
                var strategy = db.Database.CreateExecutionStrategy();
                using (var transaction = _context.Database.BeginTransaction())
                {
                    var item = await _context.Items
                                           .Where
                                           (
                                                x =>
                                                    x.Title.ToLower() == title.ToLower()
                                            )
                                            .AsNoTracking()
                                            .ToListAsync();

                        transaction.Commit();
                    return item.FirstOrDefault();
                }



            }


        }
    }
}